---
title: "15th of February, 516"
description: "15th of February, 516"
draft: false
tags: [journal]
categories: [vintage-story]
order: 12
---
I measured the courtyard yet again, just for something to do. There has to be someone I can ask about parchment for now, but even then I don't have any type of ruler for the scaling.
