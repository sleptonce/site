---
title: dog
description: "the dog in the house"
draft: false
tags: []
categories: [writing]
---
head in lap muddy paws on the cushions. i want to leave my marks everywhere like teeth marks on the cabinets and scratches on the door.

the paints peeling. look at it. the wood is ugly and unfinished underneath. that's what the paint was hiding, and i tore it off. because i knew you were on the other side. because i missed you. because you're what feeds me because you water me and because you're everything.

i just wish i wasn't like this. and at everything i do you have the right to put me down and end it all but i don't want to do this. i don't want to howl at the door every day and i don't want to scrabble on the tiles because i think i hear the garage. i know i'm an animal. please forgive me for it.