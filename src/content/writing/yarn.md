---
title: yarn
description: "Unravelling thoughts"
draft: false
tags: []
categories: [writing]
---
<p>So you can imagine a ball of yarn.</p>

<img class="block m-auto" src="/image/yarn/1.png" alt="Circle with 'yarn' written in the center of it."/>

<p>That's really tangled and stuff. And if you can't picture it, you've probably at least seen a tangled bit of string, wrapped around itself in knots and knots.</p>

<img class="block m-auto" src="/image/yarn/2.png" alt="A messy drawing of very tangled earbuds, accompanied by the text 'average earbuds after time in pocket.'"/>

<p>SOMETIMES that yarn happens to be... pulled out and the knots tied tightly at little points.</p>

<p>LIKE.</p>

<img class="block m-auto" src="/image/yarn/3.png" alt="A drawing of three loosly knotted tangles of yarn, with a red line pointing to each saying 'knots.'"/>

<p>But other times (and these are the times that suck) the yarn is suuuper compressed. Like sooooo compressed. And you start picking at it because you need the yarn or you have nothing better to do or because WHY KNOT. you know.</p>

<img class="block m-auto" src="/image/yarn/4.png" alt="A drawing of a very small, dense ball of yarn with a tail end poking out. A red line labels the tail with 'we got lucky this time there's a free end.'"/>

<p>And- And you keep pulling at it. The yarn's going to be untangled even if you have to sit there for hours. BECAUSE.</p>

<p>And you're taking it apart and there's just so much yarn. Like so much yarn you're not sure why you bought that much, or how it fit into this tiny ball.</p>

<img class="block m-auto" src="/image/yarn/5.png" alt="A drawing of a still very dense ball of yarn, roughly the same size as the previous drawing. An arrow pointing to it says 'same size???' A long tail of yarn is attached, winding back and forth a few times. A red bracket labels the tail as 'yds. of rescued yarn.'"/>

<p>The yarn's-</p>

<p>Okay, look, this isn't about yarn at all. I don't know how to keep the metaphor going. This is about feeling so much it feels like you're on fire.</p>

<div class="flex flex-col md:flex-row">
    <div class="w-4/5 md:flex-col md:justify-evenly md:flex">
        <p>Because like, look, like, there's a lot of shit in here. AND THERE'S SO MUCH! LIKE YOU COULD MAKE ANOTHER GUY WITH ALL THIS. (which i might add that i have. many times over).</p>
        <p>SO there's you in there, and any of the Guys who may have been made along the way, and you're still on fire by the way, in case you forgot. </p>
        <p>Because why WOULD the fire ever go out. You're afraid of that happening also.</p>
        <p>And so anyway it's you (and the other yous) and you've figured out that you can set other people on fire. Which it turns out they don't like. SO, YOU FIGURE, that you'll stop. Setting people on fire. And you kinda notice, its like,</p>
    </div>	
    <div class="inline-block m-auto">
        <img class="block m-auto" src="/image/yarn/6.png" alt="A messy drawing of a brain. A red line points to the brain, labeling it 'knots.'"/>
        <img class="block m-auto" src="/image/yarn/7.png" alt="The same brain drawing is repeated three times down the side, the first of which has several light colored ghost outlines. The second two are light grey. A red bracket labels the three brains as 'yds. of rescued yarn.'"/>
    </div>
</div>

<p>It's if they get really close to you. Like really close. OR alternatively, it's when you've latched onto them so fiercely it's a miracle you're separate at all. OBVIOUSLY if you put something in fire it'll probably catch. WHICH MEANS that you need to stop.</p>

<p>OKAY so you've figured that out. Now what. Because .</p>

<p>LIKE YOU KNOW. You KNOW that you can't just run off into the woods and never talk to anyone again</p>

<p>(YOU CAN ACTUALLY! if you're not a pansy. You are, however).</p>

<p>THE ANSWER that your brain has come up with (the guy who GOT you into all this trouble in the first place) is that you shouldn't talk to anyone ever again. WHICH, you don't need to run into the woods to do. You can just do that from the comfort of your own room. AND the more you do it the easier it is.</p>

<p>There's also a lot of yarn.</p>